#ifndef STREAM9_XDG_SETTINGS_SCANNER_HPP
#define STREAM9_XDG_SETTINGS_SCANNER_HPP

#include <stream9/string_view.hpp>
#include <stream9/errors.hpp>

namespace stream9::xdg::settings {

//
// Grammar
//
// entry_file := *(comment | group_header | entry)
//
// comment := new_line | (comment_mark *comment_char new_line)
// group_header := '[' group_name ']' new_line
// entry := key *space '=' *space value new_line
//
// comment_mark := '#'
// comment_char := <any char> - new_line
//
// group_name := +group_name_char
// group_name_char := printable - '[' - ']'
//
// key := +(printable - '=' - space)
// value := *(<any char> - space - new_line)
//
// new_line := LF (0x0a, '\n')
// ctrl_char := <0x00-0x1F, 0x7F>
// space := ' ' | '\t'
// printable := <any char> - ctrl_char

enum class next_action;

template<typename T>
concept content_handler =
    requires (T&& t, string_view s, std::error_code ec, next_action& a) {
        t.on_group_header(s, a);
        t.on_entry(s, s, a);
        t.on_comment(s, a);
        t.on_new_line(s.begin());
        t.on_error(ec, s, a);
    };

template<content_handler T>
void scan(string_view text, T&);

enum class next_action {
    proceed = 0,
    stop,
    skip_line,
    skip_group,
};

enum class errc {
    ok = 0,
    empty_group_name,
    invalid_group_name,
    empty_key,
    invalid_key,
    separator_is_expected,
    terminator_is_expected,
};

} // namespace stream9::xdg::settings

#endif // STREAM9_XDG_SETTINGS_SCANNER_HPP

#include "scanner.ipp"
