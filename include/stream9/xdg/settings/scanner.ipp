#ifndef STREAM9_XDG_SETTINGS_SCANNER_IPP
#define STREAM9_XDG_SETTINGS_SCANNER_IPP

namespace stream9::xdg::settings {

//
// implementation
//
using iterator = string_view::const_iterator;

class error_category_impl : public std::error_category
{
public:
    char const* name() const noexcept override
    {
        return "stream9::xdg::settings";
    }

    std::string message(int const ev) const override
    {
        switch (static_cast<errc>(ev)) {
            case errc::ok:
                return "ok";
            case errc::empty_group_name:
                return "empty group name";
            case errc::invalid_group_name:
                return "invalid group name";
            case errc::empty_key:
                return "empty key";
            case errc::invalid_key:
                return "invalid key";
            case errc::separator_is_expected:
                return "separator is expected";
            case errc::terminator_is_expected:
                return "terminator is expected";
            default:
                return "unknown error";
        }
    }
};

inline std::error_category const&
error_category()
{
    static error_category_impl impl;
    return impl;
}

inline std::error_code
make_error_code(errc c)
{
    return std::error_code(
        static_cast<int>(c),
        error_category()
    );
}

class stop {};
class skip_line {};
class skip_group {};

inline void
execute_action(next_action const a)
{
    switch (a) {
        case next_action::stop:
            throw stop();
        case next_action::skip_line:
            throw skip_line();
        case next_action::skip_group:
            throw skip_group();
        default:
            break;
    }
}

template<content_handler T>
class context
{
public:
    context(T& h) : m_handler { h } {}

    void emit_group_header(string_view text,
                           next_action a = next_action::proceed)
    {
        m_handler.on_group_header(text, a);
        execute_action(a);
    }

    void emit_key(string_view text,
                  next_action a = next_action::proceed)
    {
        m_key = text;
        execute_action(a);
    }

    void emit_value(string_view text,
                    next_action a = next_action::proceed)
    {
        m_handler.on_entry(*m_key, text, a);
        execute_action(a);
    }

    void emit_comment(string_view text,
                      next_action a = next_action::proceed)
    {
        m_handler.on_comment(text, a);
        execute_action(a);
    }

    void emit_new_line(string_view::const_iterator it)
    {
        m_handler.on_new_line(it);
        m_key = {};
    }

    void emit_error(errc e, string_view text,
                    next_action a = next_action::proceed)
    {
        m_handler.on_error(make_error_code(e), text, a);
        execute_action(a);
    }

private:
    T& m_handler;
    opt<string_view> m_key;
};

inline bool
is_new_line(char c)
{
    return c == '\n';
}

inline bool
is_ctrl_char(char c)
{
    return ((0x00 <= c && c <= 0x1F) || c == 0x7F);
}

inline bool
is_space(char c)
{
    return c == ' ' || c == '\t';
}

inline bool
is_printable(char c)
{
    return !is_ctrl_char(c);
}

template<content_handler T>
void
skip_to_eol(iterator& it, iterator end, context<T>& cxt)
{
    for (; it != end; ++it) {
        if (is_new_line(*it)) {
            cxt.emit_new_line(it);
            ++it;
            break;
        }
    }
}

template<content_handler T>
void
skip_space(iterator& it, iterator end, context<T>&)
{
    for (; it != end; ++it) {
        if (!is_space(*it)) break;
    }
}

template<content_handler T>
void
value(iterator& it, iterator end, context<T>& cxt)
{
    auto save = it;

    for (; it != end; ++it) {
        if (is_new_line(*it)) break;
    }

    cxt.emit_value({ save, it });
}

template<content_handler T>
void
key(iterator& it, iterator end, context<T>& cxt)
{
    if (it == end || *it == '=' || is_new_line(*it)) {
        cxt.emit_error(errc::empty_key, { it, it }, next_action::skip_line);
        return;
    }

    if (!is_printable(*it)) {
        cxt.emit_error(errc::invalid_key, { it, it + 1 }, next_action::skip_line);
        return;
    }

    auto save = it;
    ++it;

    for (; it != end; ++it) {
        if (*it == '=' || is_space(*it) || is_new_line(*it)) break;
        if (!is_printable(*it)) {
            cxt.emit_error(errc::invalid_key,
                           { it, it + 1 }, next_action::skip_line );
            return;
        }
    }

    cxt.emit_key({ save, it });
}

template<content_handler T>
bool
entry(iterator& it, iterator end, context<T>& cxt)
{
    if (it == end) return false;

    skip_space(it, end, cxt);

    key(it, end, cxt);

    skip_space(it, end, cxt);

    if (*it != '=') {
        cxt.emit_error(errc::separator_is_expected,
                       { it, it + 1 }, next_action::skip_line);
    }
    ++it;

    skip_space(it, end, cxt);

    value(it, end, cxt);

    skip_to_eol(it, end, cxt);

    return true;
}

inline bool
is_group_name_char(char c)
{
    return is_printable(c) && c != '[';
}

template<content_handler T>
void
group_name(iterator& it, iterator end, context<T>& cxt)
{
    if (it == end || is_new_line(*it)) return;

    if (*it == ']') {
        cxt.emit_error(
            errc::empty_group_name, { it, it }, next_action::skip_group);
    }

    if (!is_group_name_char(*it)) {
        cxt.emit_error(errc::invalid_group_name, { it, it + 1 },
                       next_action::skip_group );
    }

    ++it;

    for (; it != end; ++it) {
        if (*it == ']' || is_new_line(*it)) break;
        if (!is_group_name_char(*it)) {
            cxt.emit_error(errc::invalid_group_name, { it, it + 1 },
                           next_action::skip_group );
        }
    }
}

template<content_handler T>
bool
group_header(iterator& it, iterator end, context<T>& cxt)
{
    if (it == end) return false;
    if (*it != '[') return false;

    ++it;
    auto save = it;

    group_name(it, end, cxt);

    if (*it != ']') {
        cxt.emit_error(errc::terminator_is_expected,
                       { it, it + 1 }, next_action::skip_group);
    }

    cxt.emit_group_header({ save, it });
    ++it;

    skip_to_eol(it, end, cxt);

    return true;
}

template<content_handler T>
bool
comment(iterator& it, iterator end, context<T>& cxt)
{
    if (it == end) return false;
    if (*it != '#' && !is_new_line(*it)) return false;

    auto save = it;

    for (; it != end; ++it) {
        if (is_new_line(*it)) break;
    }

    cxt.emit_comment({ save, it });

    if (is_new_line(*it)) {
        cxt.emit_new_line(it);
        ++it;
    }

    return true;
}

template<content_handler T>
void
entry_file(iterator& it, iterator end, context<T>& cxt)
{
    try {
        while (it != end) {
            try {
                if (comment(it, end, cxt)) continue;
                if (group_header(it, end, cxt)) continue;
                if (entry(it, end, cxt)) continue;
            }
            catch (skip_line const&) {
                skip_to_eol(it, end, cxt);
            }
            catch (skip_group const&) {
                do {
                    skip_to_eol(it, end, cxt);
                }
                while (it != end && *it != '[');
            }
        }
    } catch (stop const&) {}
}

template<content_handler T>
void
scan(string_view text, T& h)
{
    context<T> cxt { h };

    auto it = text.begin();
    entry_file(it, text.end(), cxt);
}

} // namespace stream9::xdg::settings

namespace std {

template<>
struct is_error_code_enum<stream9::xdg::settings::errc>
    : true_type {};

} // namespace std

#endif // STREAM9_XDG_SETTINGS_SCANNER_IPP
